class ClaimSyncer < SyncerBase
  
  before_validation :integerize_claim_number
  
  def self.max_claim_int
    where("claim_number is not null").maximum(:claim_int)
  end
  
  def integerize_claim_number
    self.claim_int = claim_number.delete("-").to_i
  end
    
end
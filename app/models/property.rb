class Property < ActiveRecord::Base
  
  has_many :claims, :foreign_key => :lossAddressId
  has_many :britecore_contacts, :through => :x_properties_contacts, :foreign_key => :contactId
  has_many :x_properties_contacts, :foreign_key => :propertyId
  
  def mortgagees
    x_properties_contacts.select{|xpc| xpc.mortgagee == true}
  end
  
  def first_mortgagee
    mortgagees.size > 0 ? mortgagees[0].britecore_contact : BritecoreContact.new
  end
  
  def second_mortgagee
    mortgagees.size > 1 ? mortgagees[1].britecore_contact : BritecoreContact.new
  end
  
end
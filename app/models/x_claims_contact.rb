class XClaimsContact < ActiveRecord::Base
  self.table_name = 'x_claims_contacts'
  
  belongs_to :britecore_contact, :foreign_key => 'contactId'
  belongs_to :britecore_claim, :foreign_key => 'claimId'
  
end
class PropertyItem < ActiveRecord::Base
  
  has_many :claim_items, :foreign_key => :itemId
  belongs_to :property_sub_line, :foreign_key => :sublineInstanceId
  belongs_to :policy_type_item, :foreign_key => :itemId
  
end
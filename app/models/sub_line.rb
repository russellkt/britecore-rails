class SubLine < ActiveRecord::Base
  
  def self.inheritance_column
    :ruby_type
  end
  
  has_many :property_sub_lines, :foreign_key => :subLineId
  
  
  def rcc?
    name =~ /(Broad|Special)/
  end
end
class XPropertiesContact < ActiveRecord::Base
  self.table_name = :x_properties_contacts
  def self.inheritance_column
    :ruby_type
  end
  
  belongs_to :property, :foreign_key => :propertyId
  belongs_to :britecore_contact, :foreign_key => :contactId
  
end
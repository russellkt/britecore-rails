class PolicyTypeItem < ActiveRecord::Base
  
  def self.inheritance_column
    :ruby_type
  end
  
  has_many :property_items, :foreign_key => :itemId
  
end
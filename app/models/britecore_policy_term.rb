class BritecorePolicyTerm < ActiveRecord::Base
  self.table_name = 'policy_terms'
  
  belongs_to :britecore_policy, :foreign_key => 'policyId'
  belongs_to :bill_whom, :foreign_key => 'billWhomId', :class_name => "BritecoreContact"
end
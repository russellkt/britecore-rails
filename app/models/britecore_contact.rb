class BritecoreContact < ActiveRecord::Base
  self.table_name = 'contacts'
  
  def self.inheritance_column
    :ruby_type
  end  
  
  has_many :britecore_addresses, :foreign_key => :contactId
  has_many :britecore_claims, :through => :x_claims_contacts, :foreign_key => :contactId
  has_many :properties, :through => :x_properties_contacts, :foreign_key => :contactId
  
  def address_use
    britecore_addresses.size > 0 ? britecore_addresses[0] : BritecoreAddress.new
  end
  
end
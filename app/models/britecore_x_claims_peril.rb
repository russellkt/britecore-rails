class BritecoreXClaimsPeril < ActiveRecord::Base
  self.table_name = 'x_claims_perils'
  belongs_to :britecore_peril, :foreign_key => 'perilId'
  belongs_to :britecore_claims, :foreign_key => 'claimId'
end
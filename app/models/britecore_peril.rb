class BritecorePeril < ActiveRecord::Base
  self.table_name = 'perils'
  
  has_many :britecore_x_claims_perils, :foreign_key => 'perilId'  
  has_many :britecore_claims, :through => :britecore_x_claims_perils, :foreign_key => 'perilId'
  
  LOSS_ACCOUNTS = { "Accidental Discharge of Water" => "5150",
                    "Collapse" => "5150",
                    "Collision, Vehicular" => "5150",
                    "Fire" => "5146",
                    "Hail" => "5148",
                    "Liability Medical" => "5152",
                    "Liability Property" => "5153",
                    "Lightning" => "5146",
                    "Medical Expenses" => "5152",
                    "Other Coverage" => "5150",
                    "Theft" => "5150",
                    "Vandalism" => "5150",
                    "Weight of Ice & Snow" => "5150",
                    "Wind" => "5148"}
                    
  LAE_ACCOUNTS = { "Accidental Discharge of Water" => "5145",
                  "Collapse" => "5145",
                  "Collision, Vehicular" => "5145",
                  "Fire" => "5142",
                  "Hail" => "5144",
                  "Liability Medical" => "5155",
                  "Liability Property" => "5155",
                  "Lightning" => "5142",
                  "Medical Expenses" => "5155",
                  "Other Coverage" => "5145",
                  "Theft" => "5145",
                  "Vandalism" => "5145",
                  "Weight of Ice & Snow" => "5145",
                  "Wind" => "5144" }
  
  def loss_account
    LOSS_ACCOUNTS[name]
  end
  
  def lae_account
    LAE_ACCOUNTS[name]
  end
    
  
end
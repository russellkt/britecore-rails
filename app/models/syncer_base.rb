class SyncerBase < ActiveRecord::Base
  self.abstract_class = true
  establish_connection(:adapter => :sqlite3, :database => "db/syncer.sqlite3")
end
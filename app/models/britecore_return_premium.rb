class BritecoreReturnPremium < ActiveRecord::Base
  self.table_name = 'return_premium'
  
  belongs_to :britecore_policy, :foreign_key => 'policyId'
  belongs_to :britecore_address, :foreign_key => 'addressId'
  belongs_to :britecore_account_history, :foreign_key => 'accountHistoryId'
  
  has_many( :britecore_policy_terms, :foreign_key => 'policyId' )
  
  scope :to_import, -> { where("exported=false AND canceled=false AND transferred=false").includes(:britecore_address) }
  
  def self.mark_imported(ids)
    update_all("exported=true", "id in (#{ids.join(',')})")
  end
  
end
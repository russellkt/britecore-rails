class BritecoreClaim < ActiveRecord::Base
  self.table_name = 'claims'
  
  has_many :britecore_claim_payments, :foreign_key => :claimId 
  has_many :britecore_x_claims_perils, :foreign_key => :claimId
  has_many :claim_items, :foreign_key => :claimId
  belongs_to :property, :foreign_key => :lossAddressId
  belongs_to :britecore_policy, :foreign_key => :policyId
  has_many :britecore_contacts, :through => :x_claims_contacts, :foreign_key => 'claimId'
  has_many :x_claims_contacts, :foreign_key => :claimId
  has_many :britecore_perils, :through => :britecore_x_claims_perils, :foreign_key => 'claimId'
  
  def self.inheritance_column
    :ruby_type
  end
  
  def loss_account
    britecore_x_claims_perils.last.britecore_peril.loss_account
  end
  
  def lae_account
    britecore_x_claims_perils.last.britecore_peril.lae_account    
  end
  
  def loss_cause
    britecore_perils ? britecore_perils[0].name : ""
  end
  
  def occurred_on_string
    lossDate.strftime("%m/%d/%Y")
  end
  
  def named_insureds
    britecore_contacts.select{|bc| named_insured_ids.include?(bc.id) }
  end
  
  def named_insured_ids
    x_claims_contacts.where("namedInsured = true").collect{|xcc| xcc.contactId}
  end
  
  def first_mortgagee
    property.first_mortgagee
  end
  
  def second_mortgagee
    property.second_mortgagee
  end
    
end
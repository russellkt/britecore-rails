class PiplSearch
  API_KEY = 't2gwx3bnmhmehbvpueuq2d8n'
  PIPL_OPTIONS = { api_key: API_KEY,
                   minimum_probability: 0.7,
                   minimum_match: 0.9 }
  
  def self.search(first_name='', last_name = '', city = '', state = '')
    p = Pipl::Person.new
    p.add_field( Pipl::Name.new(first: first_name, last: last_name ) )
    client = Pipl::Client.new(PIPL_OPTIONS)
    client.search( person: p )
  end
  
  
  
end
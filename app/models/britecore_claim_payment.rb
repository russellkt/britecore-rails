class BritecoreClaimPayment < ActiveRecord::Base
  
  self.table_name = 'claim_transactions'
  def self.inheritance_column
    :ruby_type
  end
  
  belongs_to :britecore_claim, :foreign_key => 'claimId'
  
  scope :to_import, -> { where("exported=false AND voided=false").order("claimId ASC") }
  #scope :paid_during, lambda { |from,to| { :conditions => ['transactionDate BETWEEN ? AND ?', from, to], :include => 'britecore_claim' } } 
  
  def payees
    ActiveSupport::JSON.decode(selectedPayees.to_s)
  end
  
  def primary_payee
    payees[0]
  end
  
  def primary_contact
    BritecoreContact.find(primary_payee)
  end
  
  def primary_address
    primary_contact.britecore_addresses[0]
  end
  
  def account
    is_loss_payment? ? britecore_claim.loss_account : britecore_claim.lae_account
  end
  
  def is_loss_payment?
    item_with_payment['lossPaid'].to_i > 0
  end
  
  def is_adjusting_cost?
    item_with_payment['adjustingPaid'.to_i > 0]
  end
  
  def item_amount_array
    ActiveSupport::JSON.decode(itemAmounts)
  end
  
  def item_with_payment
    item_amount_array.select{|i| i['lossPaid'] != "0.00" || i['adjustingPaid'] != "0.00" || i['legalPaid'] != "0.00"}[0]
  end
  
  def item_description
    item_with_payment['lineItemName']
  end
  
  def memo_lead
    is_adjusting_cost? ? 'ADJUSTING COST ' : ''
  end
  
  def memo
    "#{memo_lead}FOR THE #{britecore_claim.loss_cause.upcase} DAMAGE CLAIMED TO POLICY# #{britecore_claim.britecore_policy.policyNumber} OCCURRING ON OR ABOUT #{britecore_claim.occurred_on_string}"
  end
  
  def additional_information
    "Claim# #{britecore_claim.claimNumber} - #{item_description}"
  end
  
  def self.mark_imported(ids)
    update_all("exported=true", "id in (#{ids.join(',')})")
  end
end
class PropertySubLine < ActiveRecord::Base
  
  has_many :sub_lines, :foreign_key => :subLineId
  has_many :property_items, :foreign_key => :sublineInstanceId
  
end
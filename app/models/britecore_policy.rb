class BritecorePolicy < ActiveRecord::Base
  self.table_name = 'policies'
  
  has_many :return_premium
  has_many :britecore_policy_terms, :foreign_key => :policyId
  has_many :britecore_claims
  
  def to_s
    policyNumber.to_s
  end
  
  def last_britecore_policy_term
    britecore_policy_terms.last
  end
end
class BritecoreAddress < ActiveRecord::Base
  self.table_name = 'addresses'
  def self.inheritance_column
    :ruby_type
  end
  
  belongs_to :britecore_contact, :foreign_key => :contactId
  has_many :britecore_claims, :foreign_key => :lossAddressId
  def address_line2
    addressLine2.blank? ? city_state_zip : addressLine2
  end
  
  def address_line3
    addressLine2.blank? ? "" : city_state_zip
  end
  
  def city_state_zip
    "#{addressCity}, #{addressState} #{addressZip}"
  end
end
xml.instruct!
xml.Claims do
  @claims.each_with_index do |claim,i|
    xml.Claim do
      estimateId = @estimateId
      begin
        xml.ClaimData do
          xml.ID i + @claimId
          xml.FileNumber claim.claimNumber
          xml.ClaimNumber claim.claimNumber
          xml.PolicyNo claim.britecore_policy
          xml.EffectiveDate claim.britecore_policy.britecore_policy_terms.last.effectiveDate
          xml.ExpirationDate claim.britecore_policy.britecore_policy_terms.last.expirationDate
          xml.Coinsurance 100
          xml.FilingDeadline 0
          xml.InsuredInterest
          xml.PolicyChanges
          xml.ReportDate 
          xml.NextReport
          xml.Status 'First'
          xml.DateAssigned 
          xml.DateContacted
          xml.DateInspected
          xml.DateClosed
          xml.LossDate claim.lossDate.strftime("%Y-%m-%d")
          xml.LossTime 
          xml.LossCause claim.loss_cause
          xml.CATNo
          xml.LossComments claim.description
          xml.LossSameAsMailing
          xml.RC 0
          xml.DEP 0
          xml.ACV 0 
          xml.RCClaim 0
          xml.ACVClaim 0 
          xml.Holdback 0
          xml.TotalLoss 0
          xml.Reserves 
          xml.TotalInsBLDG
          xml.TotalInsCNTS
          xml.PropertyRC
          xml.ACV
          xml.CoversheetComment
          xml.AssignementXML
          xml.EstimateDisclaimer
          xml.AdditionalPaymentAgreement
          xml.DefaultDepreciation
          xml.BlanketDed
          xml.BlanketDedAmnt
        end
        claim.claim_items.each do |ci|
          xml.Coverages do
            xml.Coverage do
              xml.ID estimateId
              xml.ClaimID i + @claimId
              xml.LineNo 0
              xml.Description
              #TODO
              xml.CoverageType 'BLDG'
              #TODO
              xml.CoverageDescription 'DWELLING'
              xml.Limit ci.property_item.limit
              xml.IncludeInTotalIns 'True'
              #TODO
              xml.RCC 'False'
              xml.Deductible ci.coverageDeductible
              xml.DedShared 'False'
              xml.DedSharedWith 0
              xml.DedNone 'False'
              xml.Reserve ci.lossReserve
              xml.DedType 0 
              xml.DedPercent 0
              xml.Estimate do 
                xml.ID estimateId
                xml.ClaimID i + @claimId
                xml.RC 0
                xml.RDep 0 
                xml.NRDep 0
                xml.ACV 0
                xml.Overhead 0
                xml.Profit 0
                xml.Tax 0
                xml.TaxRDep 0
                xml.TaxNRDep 0
                xml.OverheadRDep 0
                xml.ProfitRDep 0
                xml.ProfitNRDep 0
                #TODO
                xml.RCC 'False'
                xml.PropertyValue 0
              end
            end
            estimateId += 1
          end
        end
        xml.Contacts do 
          #Policy Address 2
          xml.Contact do
            xml.ID @contactId + 2
            xml.ContactTypeID 2
            xml.CompanyName
            xml.Email
            xml.FAX
            xml.FirstName 
            xml.LastName claim.named_insureds[0].name
            xml.MiddleName
            xml.NickName
            xml.Line1 claim.property.addressLine1
            xml.Line2
            xml.Line3
            xml.Line4
            xml.City claim.property.addressCity
            xml.StateProvince claim.property.addressState
            xml.PostalCode claim.property.addressZip
            xml.County
            xml.Country
            xml.Phone1
            xml.Phone1Type
            xml.Phone2
            xml.Phone2Type
            xml.Prefix
            xml.Suffix
            xml.TaxID
            xml.Title
            xml.UniqueName
          end
          #Insured 1 1
          xml.Contact do
            xml.ID @contactId + 1
            xml.ContactTypeID 1
            xml.CompanyName
            xml.Email
            xml.FAX
            xml.FirstName 
            xml.LastName claim.named_insureds[0].name
            xml.MiddleName
            xml.NickName
            xml.Line1 claim.named_insureds[0].britecore_addresses[0].addressLine1
            xml.Line2 
            xml.Line3
            xml.Line4
            xml.City claim.named_insureds[0].britecore_addresses[0].addressCity
            xml.StateProvince claim.named_insureds[0].britecore_addresses[0].addressState
            xml.PostalCode claim.named_insureds[0].britecore_addresses[0].addressZip
            xml.County
            xml.Country
            xml.Phone1
            xml.Phone1Type
            xml.Phone2
            xml.Phone2Type
            xml.Prefix
            xml.Suffix
            xml.TaxID
            xml.Title
            xml.UniqueName
          end  
          #Other Contact 3
          xml.Contact do
            xml.ID @contactId + 3
            xml.ContactTypeID 3
            xml.CompanyName
            xml.Email
            xml.FAX
            xml.FirstName 
            xml.LastName 
            xml.MiddleName
            xml.NickName
            xml.Line1 
            xml.Line2 
            xml.Line3
            xml.Line4
            xml.City 
            xml.StateProvince 
            xml.PostalCode 
            xml.County
            xml.Country
            xml.Phone1
            xml.Phone1Type
            xml.Phone2
            xml.Phone2Type
            xml.Prefix
            xml.Suffix
            xml.TaxID
            xml.Title
            xml.UniqueName
          end
          #BMIC 5,6,7
          [5,6,7].each do |b|
            xml.Contact do
              xml.ID @contactId + b
              xml.ContactTypeID b
              xml.CompanyName "BALDWIN MUTUAL INSURANCE CO, INC"
              xml.Email
              xml.FAX "251-943-6335"
              xml.FirstName 
              xml.LastName 
              xml.MiddleName
              xml.NickName
              xml.Line1 "315 E LAUREL AVE"
              xml.Line2 "PO DRAWER 610"
              xml.Line3
              xml.Line4
              xml.City "FOLEY"
              xml.StateProvince "AL"
              xml.PostalCode "36536"
              xml.County
              xml.Country
              xml.Phone1 "1-877-943-8526"
              xml.Phone1Type ""
              xml.Phone2 
              xml.Phone2Type ""
              xml.Prefix
              xml.Suffix
              xml.TaxID
              xml.Title
              xml.UniqueName
            end
          end
          #Blank Contacts 4,8,9
          [4,8,9].each do |blank|
            xml.Contact do
              xml.ID @contactId + blank
              xml.ContactTypeID blank
              xml.CompanyName 
              xml.Email
              xml.FAX
              xml.FirstName 
              xml.LastName 
              xml.MiddleName
              xml.NickName
              xml.Line1 
              xml.Line2 
              xml.Line3
              xml.Line4
              xml.City 
              xml.StateProvince 
              xml.PostalCode 
              xml.County
              xml.Country
              xml.Phone1 
              xml.Phone1Type 
              xml.Phone2 
              xml.Phone2Type 
              xml.Prefix
              xml.Suffix
              xml.TaxID
              xml.Title
              xml.UniqueName
            end
          end
          #Mortgagee 1 10
          xml.Contact do
            xml.ID @contactId + 10
            xml.ContactTypeID 10
            xml.CompanyName claim.first_mortgagee.name
            xml.Email
            xml.FAX
            xml.FirstName 
            xml.LastName 
            xml.MiddleName
            xml.NickName
            xml.Line1 claim.first_mortgagee.address_use.addressLine1
            xml.Line2 claim.first_mortgagee.address_use.addressLine2
            xml.Line3
            xml.Line4
            xml.City claim.first_mortgagee.address_use.addressCity
            xml.StateProvince claim.first_mortgagee.address_use.addressState
            xml.PostalCode claim.first_mortgagee.address_use.addressZip
            xml.County 
            xml.Country
            xml.Phone1 
            xml.Phone1Type 
            xml.Phone2 
            xml.Phone2Type 
            xml.Prefix
            xml.Suffix
            xml.TaxID
            xml.Title
            xml.UniqueName
          end
          #Mortgagee 2
          xml.Contact do 11
            xml.ID @contactId + 11
            xml.ContactTypeID 11
            xml.CompanyName claim.second_mortgagee.name
            xml.Email
            xml.FAX
            xml.FirstName 
            xml.LastName 
            xml.MiddleName
            xml.NickName
            xml.Line1 claim.second_mortgagee.address_use.addressLine1
            xml.Line2 claim.second_mortgagee.address_use.addressLine2
            xml.Line3
            xml.Line4
            xml.City claim.second_mortgagee.address_use.addressCity
            xml.StateProvince claim.second_mortgagee.address_use.addressState
            xml.PostalCode claim.second_mortgagee.address_use.addressZip
            xml.County
            xml.Country
            xml.Phone1 
            xml.Phone1Type 
            xml.Phone2 
            xml.Phone2Type 
            xml.Prefix
            xml.Suffix
            xml.TaxID
            xml.Title
            xml.UniqueName
          end
        end
        ClaimSyncer.create(:claim_number => claim.claimNumber, :britecore_claim_id => claim.id) if claim.claimNumber
      rescue
        puts 'error'
      end
    end
  end
end
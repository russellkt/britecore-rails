class BritecoreClaimsController < ApplicationController
  
  def index
    @last_sync = ClaimSyncer.max_claim_int
    @claims = BritecoreClaim.where("active = true and REPLACE(claimNumber,'-','') > #{@last_sync}")
  end

  def search
    claim_number = params[:claim_number]
    @claim = BritecoreClaim.find_by(:claimNumber => claim_number)
  end
  
  def powerclaim_xml
    request.format = "xml" unless params[:format]
    @claims = BritecoreClaim.find(params[:claims])
    ids = PowerclaimIdRetriever.ids
    @claimId = ids['claimId'].to_i + 1
    @contactId = ids['contactId'].to_i + 1
    @recapId = ids['recapId'].to_i + 1
    @estimateId = ids['estimateId'].to_i + 1
    stream = render_to_string(:template=>"britecore_claims/powerclaim_xml.xml" )  
    send_data(stream, :type=>"text/xml",:filename => "powerclaim.xml")
  end

end

desc "Migrate the database through scripts in db/migrate directory."

namespace :db do

  task :migrate_syncer do
    ActiveRecord::Base.establish_connection(:adapter => :sqlite3, :database => "db/syncer.sqlite3")
    ActiveRecord::Migrator.migrate("db/migrate/syncer/")
  end

end
require 'test_helper'

class PowerclaimIdRetrieverTest < ActiveSupport::TestCase
  
  test "retrieves ids from remote site" do 
    ids = PowerclaimIdRetriever.ids
    assert ids['claimId']
    assert ids['contactId']
  end

end
require 'test_helper'
require 'pipl'
class PiplSearchTest < ActiveSupport::TestCase
  
  test "retrieves address from pipl search" do 
    response = PiplSearch.search('Kevin', 'Russell', 'Birmingham', 'AL')
    puts response.possible_persons.collect{|p| p.names.first}
    puts "person: #{response.person}"
    assert response
  end

end